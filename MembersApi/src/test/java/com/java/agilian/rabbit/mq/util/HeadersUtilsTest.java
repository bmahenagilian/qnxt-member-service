package com.java.agilian.rabbit.mq.util;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
@Ignore
public class HeadersUtilsTest {

    @Test
    public final void testGetBasicAuthenticationHeader() {

        final HttpHeaders headers=HeadersUtils.getBasicAuthenticationHeader("name", "password");

        Assert.assertEquals(Arrays.asList("Basic bmFtZTpwYXNzd29yZA=="), headers.get("Authorization"));
    }

    @Test
    public final void testgGetBearerHeader() {

        final HttpHeaders headers=HeadersUtils.getBearerHeader( "token");

        Assert.assertEquals(Arrays.asList("Bearer token"), headers.get("Authorization"));
    }
}
package com.java.agilian.rabbit.mq.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
public class ProcessMetadataTest {

    @Test
    public final void testSetAndGet() {

        final List<ProcessMessage> messages =  Arrays.asList(new ProcessMessage(), new ProcessMessage(), new ProcessMessage());

        final ProcessMetadata metadata =  new ProcessMetadata();
        metadata.setProcessMessages(messages);
        metadata.setConversationID("id");
        metadata.setTransactionID("tid");

        Assert.assertEquals(3, metadata.getProcessMessages().size());
        Assert.assertEquals("id", metadata.getConversationID());
        Assert.assertEquals("tid", metadata.getTransactionID());
    }
}
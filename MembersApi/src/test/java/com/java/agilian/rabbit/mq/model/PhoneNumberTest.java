package com.java.agilian.rabbit.mq.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
public class PhoneNumberTest {

    @Test
    public final void testSetGet() {

        final PhoneNumber number = new PhoneNumber();
        number.setNumber("number");
        number.setType("type");

        Assert.assertEquals("number", number.getNumber());
        Assert.assertEquals("type", number.getType());

    }
}
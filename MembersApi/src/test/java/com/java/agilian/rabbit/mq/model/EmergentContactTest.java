package com.java.agilian.rabbit.mq.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
public class EmergentContactTest {

    @Test
    public final void testSetAndGet(){

        final EmergentContact contact = new EmergentContact();
        contact.setName("name");
        contact.setRelationship("relation");

        Assert.assertEquals("name", contact.getName());
        Assert.assertEquals("relation", contact.getRelationship());
    }
}
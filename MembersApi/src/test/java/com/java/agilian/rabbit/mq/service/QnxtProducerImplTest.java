package com.java.agilian.rabbit.mq.service;

import com.java.agilian.rabbit.mq.service.impl.QnxtProducerServiceImpl;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
@Ignore
@ExtendWith(MockitoExtension.class)
public class QnxtProducerImplTest {

    /*@Mock
    private ConnectionFactory factory;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private QnxtProducerServiceImpl service;

    @BeforeEach
    void beforeEachTest() {
        service.setUsername("user name");
        service.setPort(12);
        service.setHost("host");
        service.setExchange("exchange");
        service.setPwd("pwd");
        service.setRoutingkey("key");
    }

    @Test
    public final void testSend() throws IOException, TimeoutException, NoSuchAlgorithmException, KeyManagementException {

        final Connection connection = mock(Connection.class);
        final Channel channel = mock(Channel.class);

        when(factory.newConnection()).thenReturn(connection);
        when(connection.createChannel()).thenReturn(channel);

        service.send("message");
    }

    @Test
    public final void testGetMemberWhenResponseNotFound() throws Exception {

        final ResponseEntity<String> responseEntity = mock(ResponseEntity.class);

        final Connection connection = mock(Connection.class);
        final Channel channel = mock(Channel.class);

        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())

        ).thenReturn(responseEntity);

        when(responseEntity.getBody()).thenReturn("");

        //service.getMember("memberid");
    }

    @Test
    public final void testGetMemberWhenAccessTokenNotFound() throws Exception {

        final ResponseEntity<String> responseEntity = mock(ResponseEntity.class);

        final Connection connection = mock(Connection.class);
        final Channel channel = mock(Channel.class);

        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())

        ).thenReturn(responseEntity);

        when(responseEntity.getBody()).thenReturn("{\"data\" :\"token\"}");

        //service.getMember("memberid");
    }

    @Test
    public final void testGetMember() throws Exception {

        final ResponseEntity<String> responseEntity = mock(ResponseEntity.class);

        final Connection connection = mock(Connection.class);
        final Channel channel = mock(Channel.class);

        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())

        ).thenReturn(responseEntity);

        when(responseEntity.getBody()).thenReturn("{\"access_token\" :\"token\"}");

        when(factory.newConnection()).thenReturn(connection);
        when(connection.createChannel()).thenReturn(channel);

       // service.getMember("memberid");
    }*/
}
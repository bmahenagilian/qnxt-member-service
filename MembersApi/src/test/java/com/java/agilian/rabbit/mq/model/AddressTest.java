package com.java.agilian.rabbit.mq.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
public class AddressTest {

    @Test
    public final void testGetSet() {

        final Address address = new Address();

        address.setCountryName("country");
        address.setType("type");
        address.setAttentionTo("attention to");
        address.setAddressLine1("line1");
        address.setAddressLine2("line2");
        address.setCity("city");
        address.setState("state");
        address.setZipCode("zip code");
        address.setCounty("country");
        address.setPostalCode("code");
        address.setProvince("province");
        address.setCountryID("id");

        Assert.assertEquals("country", address.getCountryName());
        Assert.assertEquals("type", address.getType());
        Assert.assertEquals("attention to", address.getAttentionTo());
        Assert.assertEquals("line1", address.getAddressLine1());
        Assert.assertEquals("line2", address.getAddressLine2());
        Assert.assertEquals("city", address.getCity());
        Assert.assertEquals("state", address.getState());
        Assert.assertEquals("zip code", address.getZipCode());
        Assert.assertEquals("country", address.getCounty());
        Assert.assertEquals("code", address.getPostalCode());
        Assert.assertEquals("province", address.getProvince());
        Assert.assertEquals("id", address.getCountryID());
    }
}
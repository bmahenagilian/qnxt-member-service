package com.java.agilian.rabbit.mq.controller;

import com.java.agilian.rabbit.mq.service.MemberService;
import com.java.agilian.rabbit.mq.service.impl.QnxtConsumerServiceImpl;
import com.java.agilian.rabbit.mq.service.impl.QnxtProducerServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
@ExtendWith(MockitoExtension.class)
public class MembersApiControllerTest {

    @Mock
    private QnxtProducerServiceImpl producerService;

    @Mock
    private QnxtConsumerServiceImpl consumerService;

    @Mock
    private MemberService memberService;

    @InjectMocks
    private MembersApiController controller;

    /*@Test
    public final void testGetMember() throws Exception {

        final HttpServletRequest request = mock(HttpServletRequest.class);

        when(memberService.getMemberById("member-id")).thenReturn("data");

        final ResponseEntity<String> response = controller.getMemberById(request, "member-id");

        Assert.assertEquals("data", response.getBody());
        Assert.assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public final void testConsumer() throws Exception {

        final HttpServletRequest request = mock(HttpServletRequest.class);

        when(consumerService.receive()).thenReturn("data");

        final ResponseEntity<String> response = controller.consumer(request);

        Assert.assertEquals("data", response.getBody());
        Assert.assertEquals(200, response.getStatusCodeValue());
    }*/
}
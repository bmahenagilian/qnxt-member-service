package com.java.agilian.rabbit.mq.service;

import com.java.agilian.rabbit.mq.entity.MemberEntity;
import com.java.agilian.rabbit.mq.repository.MembersRepository;
import com.java.agilian.rabbit.mq.service.impl.QnxtConsumerServiceImpl;
import com.rabbitmq.client.*;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */

@Ignore
@ExtendWith(MockitoExtension.class)
public class QnxtConsumerImplTest {

   /* @Mock
    private ConnectionFactory factory;

    @Mock
    private MembersRepository membersRepository;

    @InjectMocks
    private QnxtConsumerServiceImpl service;

    @BeforeEach
    void beforeEachTest() {
        service.setUsername("user name");
        service.setPort(12);
        service.setHost("host");
        service.setExchange("exchange");
        service.setPwd("pwd");
        service.setRoutingkey("key");
        service.setQueue("queue");
    }

    @Test
    public final void testReceiveAll() throws Exception {

        final Connection connection = mock(Connection.class);
        final Channel channel = mock(Channel.class);

        when(factory.newConnection()).thenReturn(connection);
        when(connection.createChannel()).thenReturn(channel);

        service.receiveAll();
    }

    @org.junit.Test(expected = RuntimeException.class)
    public final void testReceiveAllWhenExceptionOccures() throws Exception {

        final Connection connection = mock(Connection.class);
        final Channel channel = mock(Channel.class);

        when(factory.newConnection()).thenReturn(connection);
        when(connection.createChannel()).thenReturn(channel);

        when(channel.queueDeclare(anyString(), true, false, false, null)).thenThrow(new RuntimeException("run time exception"));

        service.receiveAll();
    }

    @Test
    public final void testReceiveWhenResponseNotFound() throws Exception {

        final Connection connection = mock(Connection.class);
        final Channel channel = mock(Channel.class);

        when(factory.newConnection()).thenReturn(connection);
        when(connection.createChannel()).thenReturn(channel);
        when(channel.basicGet(anyString(), anyBoolean())).thenReturn(null);

        Assert.assertEquals(StringUtils.EMPTY, service.receive());
    }

    @Test
    public final void testReceive() throws Exception {

        final Connection connection = mock(Connection.class);
        final Channel channel = mock(Channel.class);
        final GetResponse response =mock(GetResponse.class);
        final Envelope envelope = mock(Envelope.class);

        final byte[] body = "response body".getBytes();

        when(factory.newConnection()).thenReturn(connection);
        when(connection.createChannel()).thenReturn(channel);
        when(channel.basicGet(anyString(), anyBoolean())).thenReturn(response);
        when(response.getBody()).thenReturn(body);
        when(response.getEnvelope()).thenReturn(envelope);
        when(envelope.getDeliveryTag()).thenReturn(12L);

        Assert.assertEquals("response body", service.receive());
    }

    @Test
    public final void testSaveToDateBase() throws Exception {

        final MemberEntity memberEntity = mock(MemberEntity.class);
        final String memberJson = "{\"ethnicity\":\"data\",\"language\":\"lang\",\"secondaryID\":\"second\",\"contactInfo\":{\"addresses\":null,\"phoneNumbers\":null,\"emergentContact\":null,\"email\":null,\"contactName\":null},\"memberRelations\":[{\"primaryMemID\":null,\"primaryMemName\":null,\"relMemName\":null,\"relation\":null,\"relType\":null,\"primaryHasAccess\":null,\"primaryRestrictedAccessGroups\":null,\"relHasAccess\":null,\"relRestrictedAccessGroups\":null,\"relIsVip\":null,\"relMemID\":null,\"relationID\":null,\"relationIDOriginal\":null,\"effDate\":null,\"effDateOriginal\":null,\"termDate\":null,\"entityState\":null},{\"primaryMemID\":null,\"primaryMemName\":null,\"relMemName\":null,\"relation\":null,\"relType\":null,\"primaryHasAccess\":null,\"primaryRestrictedAccessGroups\":null,\"relHasAccess\":null,\"relRestrictedAccessGroups\":null,\"relIsVip\":null,\"relMemID\":null,\"relationID\":null,\"relationIDOriginal\":null,\"effDate\":null,\"effDateOriginal\":null,\"termDate\":null,\"entityState\":null},{\"primaryMemID\":null,\"primaryMemName\":null,\"relMemName\":null,\"relation\":null,\"relType\":null,\"primaryHasAccess\":null,\"primaryRestrictedAccessGroups\":null,\"relHasAccess\":null,\"relRestrictedAccessGroups\":null,\"relIsVip\":null,\"relMemID\":null,\"relationID\":null,\"relationIDOriginal\":null,\"effDate\":null,\"effDateOriginal\":null,\"termDate\":null,\"entityState\":null}],\"processMetadata\":{\"conversationID\":null,\"transactionID\":null,\"processMessages\":null},\"genderIdentity\":\"iden\",\"hasAccess\":\"access\",\"restrictedAccessGroups\":\"groups\",\"memID\":\"memid\",\"name\":\"name\",\"entityID\":\"id\",\"sex\":\"male\",\"maritalStatus\":\"married\",\"ssn\":\"ssn\",\"birthDate\":1655765793345,\"headOfHouse\":\"house\",\"isVip\":\"yes\",\"status\":\"status\",\"deathDate\":1655765793345,\"ethnicityID\":\"eid\",\"languageID\":\"langId\",\"medianIncome\":12,\"guardian\":\"guar\",\"externalID\":\"extid\",\"isMultipleBirth\":\"n\",\"isSubscriber\":\"y\",\"genderIdentityID\":\"id\",\"entityState\":\"state\"}";

        when(membersRepository.save(any(MemberEntity.class))).thenReturn(memberEntity);

        service.saveToDatabase(memberJson);
    }*/
}
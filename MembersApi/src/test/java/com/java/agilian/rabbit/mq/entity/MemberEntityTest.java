package com.java.agilian.rabbit.mq.entity;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
public class MemberEntityTest {


    @Test
    public final void testSetGet() {

        final MemberEntity entity= new MemberEntity();
        entity.setId(1);

        Assert.assertEquals(1, entity.getId().intValue());
    }
}
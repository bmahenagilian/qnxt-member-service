package com.java.agilian.rabbit.mq.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
public class MemberTest {

    @Test
    public final void testSetAndGet() {

        final ContactInfo info = new ContactInfo();

        final List<MemberRelation> relationList = Arrays.asList(new MemberRelation(), new MemberRelation(), new MemberRelation());

        final Member member = new Member();
        member.setEthnicity("data");
        member.setLanguage("lang");
        member.setSecondaryID("second");
        member.setContactInfo(info);
        member.setMemberRelations(relationList);
        member.setProcessMetadata(new ProcessMetadata());
        member.setGenderIdentity("iden");
        member.setHasAccess("access");
        member.setRestrictedAccessGroups("groups");
        member.setMemID("memid");
        member.setEntityID("id");
        member.setSex("male");
        member.setMaritalStatus("married");
        member.setSsn("ssn");
        member.setBirthDate(new Timestamp(new Date().getTime()));
        member.setHeadOfHouse("house");
        member.setIsVip("yes");
        member.setStatus("status");
        member.setDeathDate(new Timestamp(new Date().getTime()));
        member.setEntityID("id");
        member.setLanguageID("langId");
        member.setMedianIncome(12);
        member.setGuardian("guar");
        member.setExternalID("extid");
        member.setIsMultipleBirth("n");
        member.setIsSubscriber("y");
        member.setGenderIdentityID("id");
        member.setEntityState("state");
        member.setName("name");
        member.setEthnicityID("eid");

        Assert.assertEquals("data", member.getEthnicity());
        Assert.assertEquals("lang", member.getLanguage());
        Assert.assertEquals("second", member.getSecondaryID());
        Assert.assertSame(info, member.getContactInfo());
        Assert.assertEquals(3, member.getMemberRelations().size());
        Assert.assertNotNull( member.getProcessMetadata());
        Assert.assertEquals("iden", member.getGenderIdentity());
        Assert.assertEquals("access", member.getHasAccess());
        Assert.assertEquals("groups", member.getRestrictedAccessGroups());
        Assert.assertEquals("memid", member.getMemID());
        Assert.assertEquals("id", member.getEntityID());
        Assert.assertEquals("male", member.getSex());
        Assert.assertEquals("married", member.getMaritalStatus());
        Assert.assertEquals("ssn", member.getSsn());
        Assert.assertNotNull( member.getBirthDate());
        Assert.assertEquals("house", member.getHeadOfHouse());
        Assert.assertEquals("yes", member.getIsVip());
        Assert.assertEquals("status", member.getStatus());
        Assert.assertNotNull( member.getDeathDate());
        Assert.assertEquals("id", member.getEntityID());
        Assert.assertEquals("langId", member.getLanguageID());
        Assert.assertEquals(12, member.getMedianIncome());
        Assert.assertEquals("guar", member.getGuardian());
        Assert.assertEquals("extid", member.getExternalID());
        Assert.assertEquals("n", member.getIsMultipleBirth());
        Assert.assertEquals("y", member.getIsSubscriber());
        Assert.assertEquals("id", member.getGenderIdentityID());
        Assert.assertEquals("state", member.getEntityState());
        Assert.assertEquals("name", member.getName());
        Assert.assertEquals("eid", member.getEthnicityID());
    }
}
package com.java.agilian.rabbit.mq.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
public class MemberRelationTest {

    @Test
    public final void testGetAndSet() {

        final MemberRelation relation = new MemberRelation();
        relation.setPrimaryMemID("mem");
        relation.setPrimaryMemName("name");
        relation.setRelMemName("name");
        relation.setRelation("relation");
        relation.setRelType("type");
        relation.setPrimaryHasAccess("primary");
        relation.setPrimaryRestrictedAccessGroups("group");
        relation.setRelHasAccess("access");
        relation.setRelRestrictedAccessGroups("group");
        relation.setRelIsVip("vip");
        relation.setRelMemID("memid");
        relation.setRelationID("id");
        relation.setRelationIDOriginal("original");
        relation.setEffDate(new Timestamp(new Date().getTime()));
        relation.setEffDateOriginal(new Timestamp(new Date().getTime()));
        relation.setTermDate(new Timestamp(new Date().getTime()));
        relation.setEntityState("state");

        Assert.assertEquals("mem", relation.getPrimaryMemID());
        Assert.assertEquals("name", relation.getPrimaryMemName());
        Assert.assertEquals("name", relation.getRelMemName());
        Assert.assertEquals("relation", relation.getRelation());
        Assert.assertEquals("type", relation.getRelType());
        Assert.assertEquals("primary", relation.getPrimaryHasAccess());
        Assert.assertEquals("group", relation.getPrimaryRestrictedAccessGroups());
        Assert.assertEquals("access", relation.getRelHasAccess());
        Assert.assertEquals("group", relation.getRelRestrictedAccessGroups());
        Assert.assertEquals("vip", relation.getRelIsVip());
        Assert.assertEquals("memid", relation.getRelMemID());
        Assert.assertEquals("id", relation.getRelationID());
        Assert.assertEquals("original", relation.getRelationIDOriginal());
        Assert.assertNotNull( relation.getEffDate());
        Assert.assertNotNull( relation.getEffDateOriginal());
        Assert.assertNotNull( relation.getTermDate());
        Assert.assertEquals("state", relation.getEntityState());
    }
}
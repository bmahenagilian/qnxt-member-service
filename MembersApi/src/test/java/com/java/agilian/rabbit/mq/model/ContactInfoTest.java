package com.java.agilian.rabbit.mq.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
public class ContactInfoTest {

    @Test
    public final void testSetAndGet() {

        final Address add = new Address();
        add.setCountryID("CONN");

        final Address add2 = new Address();
        add.setCountryID("CONN2");

        final List<Address> addresses = Arrays.asList(add,add2);

        final PhoneNumber number = new PhoneNumber();
        number.setNumber("num1");

        final PhoneNumber number1 = new PhoneNumber();
        number.setNumber("num2");

        final List<PhoneNumber> phoneNumbers = Arrays.asList(number,number1);

        final EmergentContact contact = new EmergentContact();

        final ContactInfo info = new ContactInfo();
        info.setContactName("name");
        info.setEmail("email");
        info.setAddresses(addresses);
        info.setEmergentContact(contact);
        info.setPhoneNumbers(phoneNumbers);

        Assert.assertEquals(2, info.getAddresses().size());
        Assert.assertEquals(2, info.getPhoneNumbers().size());
        Assert.assertSame(contact, info.getEmergentContact());
        Assert.assertEquals("email", info.getEmail());
        Assert.assertEquals("name", info.getContactName());
    }
}
package com.java.agilian.rabbit.mq.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Buddha.
 * @Date 6/20/2022.
 */
public class ProcessMessageTest {

    @Test
    public final void testSetGet() {

        final ProcessMessage message = new ProcessMessage();
        message.setMessage("message");
        message.setMessageID("id");
        message.setCategory("category");
        message.setSeverity("severity");
        message.setSource("source");
        message.setAdditionalInfo(new AdditionalInfo());

        Assert.assertEquals("message", message.getMessage());
        Assert.assertEquals("id", message.getMessageID());
        Assert.assertEquals("category", message.getCategory());
        Assert.assertEquals("severity", message.getSeverity());
        Assert.assertEquals("source", message.getSource());
        Assert.assertNotNull( message.getAdditionalInfo());}
}
package com.java.agilian.rabbit.mq.controller;

import com.java.agilian.rabbit.mq.dto.MemberDto;
import com.java.agilian.rabbit.mq.model.MemberAttributeSearch;
import com.java.agilian.rabbit.mq.service.MemberAttributeService;
import com.java.agilian.rabbit.mq.service.MemberMemoAlertService;
import com.java.agilian.rabbit.mq.service.MemberService;
import com.java.agilian.rabbit.mq.service.impl.QnxtConsumerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "member", produces = "application/json")
public class MembersApiController {

    @Autowired
    private MemberService memberService;

    @Autowired
    private MemberAttributeService memberAttributeService;

    @Autowired
    private MemberMemoAlertService memberMemoAlertService;

    @Autowired
    private QnxtConsumerServiceImpl consumer;

    @GetMapping(value = "member/{memberId}")
    public ResponseEntity<String> getMemberById(final HttpServletRequest request, @PathVariable("memberId") String memberId, @RequestBody(required = false) MemberDto memberDto) throws Exception {

        final String response = memberService.getMemberById(memberId, memberDto);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping(value = "member-attributes-search/{memberId}")
    public ResponseEntity<String> searchMemberAttributes(final HttpServletRequest request, @PathVariable("memberId") String memberId, @RequestBody(required = false) MemberAttributeSearch searchCriteria) throws Exception {

        final String response = memberAttributeService.searchMemberAttributes(memberId, searchCriteria);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @PutMapping(value = "member-attribute/{memberId}")
    public ResponseEntity<String> updateMemberAttribute(final HttpServletRequest request, @PathVariable("memberId") String memberId, @RequestBody String inputRequest) throws Exception {

        final String response = memberAttributeService.updateMemberAttributes(memberId, inputRequest);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "member-memos/{memberId}")
    public ResponseEntity<String> getMemberMemos(final HttpServletRequest request, @PathVariable("memberId") String memberId) throws Exception {

        final String response = memberMemoAlertService.getMemberMemos(memberId);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @PutMapping(value = "member-memos/{memberId}")
    public ResponseEntity<String> updateMemberMemos(final HttpServletRequest request, @PathVariable("memberId") String memberId, @RequestBody String inputRequest) throws Exception {

        final String response = memberMemoAlertService.updateMemberMemos(memberId, inputRequest);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping(value = "member-alerts/{memberId}")
    public ResponseEntity<String> getMemberAlerts(final HttpServletRequest request, @PathVariable("memberId") String memberId, @RequestParam(required = false) List<String> expand) throws Exception {

        final String response = memberMemoAlertService.getMemberAlerts(memberId, expand);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @PutMapping(value = "member-alerts/{memberId}")
    public ResponseEntity<String> updateMemberAlerts(final HttpServletRequest request, @PathVariable("memberId") String memberId, @RequestBody String inputRequest) throws Exception {

        final String response = memberMemoAlertService.updateMemberAlerts(memberId, inputRequest);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @GetMapping(value = "consumer")
    public ResponseEntity<String> consumer(final HttpServletRequest request) throws Exception {

        final String response = consumer.receive();

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }
}


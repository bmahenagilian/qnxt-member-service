package com.java.agilian.rabbit.mq.dto;

import java.util.Date;
import java.util.List;

/**
 * @author Buddha.
 * @Date 7/5/2022.
 */
public class MemberDto {

    private Date asOfDate;
    private List<String> expand;
    private String accessGroupControl;

    public Date getAsOfDate() {
        return asOfDate;
    }

    public void setAsOfDate(Date asOfDate) {
        this.asOfDate = asOfDate;
    }

    public List<String> getExpand() {
        return expand;
    }

    public void setExpand(List<String> expand) {
        this.expand = expand;
    }

    public String getAccessGroupControl() {
        return accessGroupControl;
    }

    public void setAccessGroupControl(String accessGroupControl) {
        this.accessGroupControl = accessGroupControl;
    }
}

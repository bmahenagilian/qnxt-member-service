package com.java.agilian.rabbit.mq.model;

import java.sql.Timestamp;

public class MemberRelation {
    private String primaryMemID;
    private String primaryMemName;
    private String relMemName;
    private String relation;
    private String relType;
    private String primaryHasAccess;
    private String primaryRestrictedAccessGroups;
    private String relHasAccess;
    private String relRestrictedAccessGroups;
    private String relIsVip;
    private String relMemID;
    private String relationID;
    private String relationIDOriginal;
    private Timestamp effDate;
    private Timestamp effDateOriginal;
    private Timestamp termDate;
    private String entityState;

    public String getPrimaryMemID() {
        return primaryMemID;
    }

    public void setPrimaryMemID(String value) {
        this.primaryMemID = value;
    }

    public String getPrimaryMemName() {
        return primaryMemName;
    }

    public void setPrimaryMemName(String value) {
        this.primaryMemName = value;
    }

    public String getRelMemName() {
        return relMemName;
    }

    public void setRelMemName(String value) {
        this.relMemName = value;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String value) {
        this.relation = value;
    }

    public String getRelType() {
        return relType;
    }

    public void setRelType(String value) {
        this.relType = value;
    }

    public String getPrimaryHasAccess() {
        return primaryHasAccess;
    }

    public void setPrimaryHasAccess(String value) {
        this.primaryHasAccess = value;
    }

    public String getPrimaryRestrictedAccessGroups() {
        return primaryRestrictedAccessGroups;
    }

    public void setPrimaryRestrictedAccessGroups(String value) {
        this.primaryRestrictedAccessGroups = value;
    }

    public String getRelHasAccess() {
        return relHasAccess;
    }

    public void setRelHasAccess(String value) {
        this.relHasAccess = value;
    }

    public String getRelRestrictedAccessGroups() {
        return relRestrictedAccessGroups;
    }

    public void setRelRestrictedAccessGroups(String value) {
        this.relRestrictedAccessGroups = value;
    }

    public String getRelIsVip() {
        return relIsVip;
    }

    public void setRelIsVip(String value) {
        this.relIsVip = value;
    }

    public String getRelMemID() {
        return relMemID;
    }

    public void setRelMemID(String value) {
        this.relMemID = value;
    }

    public String getRelationID() {
        return relationID;
    }

    public void setRelationID(String value) {
        this.relationID = value;
    }

    public String getRelationIDOriginal() {
        return relationIDOriginal;
    }

    public void setRelationIDOriginal(String value) {
        this.relationIDOriginal = value;
    }


    public Timestamp getEffDate() {
        return effDate;
    }

    public void setEffDate(Timestamp effDate) {
        this.effDate = effDate;
    }

    public Timestamp getEffDateOriginal() {
        return effDateOriginal;
    }

    public void setEffDateOriginal(Timestamp effDateOriginal) {
        this.effDateOriginal = effDateOriginal;
    }

    public Timestamp getTermDate() {
        return termDate;
    }

    public void setTermDate(Timestamp value) {
        this.termDate = value;
    }

    public String getEntityState() {
        return entityState;
    }

    public void setEntityState(String value) {
        this.entityState = value;
    }
}
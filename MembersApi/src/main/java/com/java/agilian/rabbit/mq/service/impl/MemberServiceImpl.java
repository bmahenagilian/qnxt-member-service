package com.java.agilian.rabbit.mq.service.impl;

import com.java.agilian.rabbit.mq.dto.MemberDto;
import com.java.agilian.rabbit.mq.service.MemberService;
import com.java.agilian.rabbit.mq.util.HeadersUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;

import static com.java.agilian.rabbit.mq.service.ApiConstants.GET_MEMBER;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Implementation of {@link com.java.agilian.rabbit.mq.service.MemberService}
 *
 * @author Buddha.
 * @Date 6/30/2022.
 */
@Service
public class MemberServiceImpl implements MemberService {

    private static final Logger log = LoggerFactory.getLogger(MemberServiceImpl.class);

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private QnxtProducerServiceImpl producerService;

    @Override
    public String getMemberById(final String memberId, final MemberDto memberDto) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(GET_MEMBER, memberId), HttpMethod.GET, new HttpEntity<String>(httpHeaders), String.class);

        final String responseBody = apiResponse.getBody();

        if (isEmpty(apiResponse.getBody())) {
            return EMPTY;
        }

        // sending to message queue.
        producerService.send(responseBody);

        return responseBody;
    }
}

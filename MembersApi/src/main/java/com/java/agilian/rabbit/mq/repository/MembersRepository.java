package com.java.agilian.rabbit.mq.repository;

import com.java.agilian.rabbit.mq.entity.MemberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Members entity to store Members api response.
 *
 * @author Buddha.
 * @Date 6/16/2022.
 */
@Repository
public interface MembersRepository extends JpaRepository<MemberEntity, Long> {


}

package com.java.agilian.rabbit.mq.service;

import com.java.agilian.rabbit.mq.model.MemberAttributeSearch;

/**
 * Member Attribute Service that process member attribute api's.
 *
 * @author Buddha.
 * @Date 6/30/2022.
 */
public interface MemberAttributeService {

    /**
     * Search the member attributes for the given member id.
     *
     * @param memberId       member id.
     * @param searchCriteria search criteria.
     * @return member attributes.
     * @throws Exception exception.
     */
    String searchMemberAttributes(final String memberId, final MemberAttributeSearch searchCriteria) throws Exception;

    /**
     * Update member attributes for the given member id.
     *
     * @param memberId     member id.
     * @param inputRequest input request.\
     * @return updated response.
     * @throws Exception exception.
     */
    String updateMemberAttributes(final String memberId, final String inputRequest) throws Exception;
}

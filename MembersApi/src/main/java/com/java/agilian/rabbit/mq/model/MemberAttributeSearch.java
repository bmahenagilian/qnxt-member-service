package com.java.agilian.rabbit.mq.model;

import java.util.Date;
import java.util.List;

/**
 * Model class that stores member attribute search information.
 *
 * @author Buddha.
 * @Date 7/1/2022.
 */
public class MemberAttributeSearch {

    private String attributeId;
    private Date asOfDate;
    private Integer skip;
    private Integer take;
    private List<String> orderBy;
    private List<String> expand;

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public Date getAsOfDate() {
        return asOfDate;
    }

    public void setAsOfDate(Date asOfDate) {
        this.asOfDate = asOfDate;
    }

    public Integer getSkip() {
        return skip;
    }

    public void setSkip(Integer skip) {
        this.skip = skip;
    }

    public Integer getTake() {
        return take;
    }

    public void setTake(Integer take) {
        this.take = take;
    }

    public List<String> getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(List<String> orderBy) {
        this.orderBy = orderBy;
    }

    public List<String> getExpand() {
        return expand;
    }

    public void setExpand(List<String> expand) {
        this.expand = expand;
    }
}

package com.java.agilian.rabbit.mq.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.agilian.rabbit.mq.entity.MemberEntity;
import com.java.agilian.rabbit.mq.model.Member;
import com.java.agilian.rabbit.mq.repository.MembersRepository;
import com.rabbitmq.client.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Date;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * Consumer class that consumes data from message queue.
 */
@Service
public class QnxtConsumerServiceImpl {

    public static final String QUE_PROTOCOL = "TLSv1.2";
    private static final Logger log = LoggerFactory.getLogger(QnxtConsumerServiceImpl.class);
    @Value("${spring.rabbitmq.username}")
    private String username;
    @Value("${spring.rabbitmq.password}")
    private String pwd;
    @Value("${spring.rabbitmq.host}")
    private String host;

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;
    @Value("${spring.rabbitmq.queue}")
    private String queue;
    @Value("${spring.rabbitmq.port}")
    private Integer port;

    @Value("${spring.rabbitmq.routingkey}")
    private String routingkey;

    private ConnectionFactory factory = new ConnectionFactory();

    @Autowired
    private MembersRepository membersRepository;

    public String receive() throws Exception {

        receiveAll();
        final boolean autoAck = false;
        factory.setUsername(username);
        factory.setPassword(pwd);

        factory.setHost(host);
        factory.setPort(port);
        // Allows client to establish a connection over TLS
        factory.useSslProtocol(QUE_PROTOCOL);

        // Create a connection
        final Connection conn = factory.newConnection();

        // Create a channel
        final Channel channel = conn.createChannel();

        final DefaultConsumer c = new DefaultConsumer(channel);

        final GetResponse response = channel.basicGet(queue, autoAck);
        channel.basicConsume(queue, true, c);

        if (response == null) {
            log.info("No Messages Available in Queue:");
            return EMPTY;
        }

        final AMQP.BasicProperties props = response.getProps();
        final byte[] body = response.getBody();
        final String message = new String(body);


        log.info("Message received from queue: " + message);

        final long deliveryTag = response.getEnvelope().getDeliveryTag();
        channel.basicAck(deliveryTag, false); // acknowledge receipt of the message

        // saving to csv file
        writeCsv(message);

        // saving to database.
        //saveToDatabase(message);

        return message;
    }

    public void receiveAll() throws Exception {

        final boolean autoAck = false;
        factory.setUsername(username);
        factory.setPassword(pwd);

        //Replace the URL with your information
        factory.setHost(host);
        factory.setPort(port);
        // Allows client to establish a connection over TLS
        factory.useSslProtocol(QUE_PROTOCOL);

        // Create a connection
        final Connection conn = factory.newConnection();

        // Create a channel
        final Channel channel = conn.createChannel();

        try {
            channel.queueDeclare(queue, true, false, false, null);

            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {

                    String message = new String(body, StandardCharsets.UTF_8);
                    log.info("Received Message --> " + message);
                }
            };
        } catch (Exception e) {
            log.error("Consume Exception", e);
            throw e;
        }
    }

    /**
     * Saving to database.
     *
     * @param response api response
     */
    public void saveToDatabase(final String response) throws Exception {

        final Member member = new ObjectMapper().readValue(response, Member.class);

        final MemberEntity entity = new MemberEntity();

        membersRepository.save(entity);
    }

    /**
     * Writing into CSV file.
     *
     * @param response api response.
     * @throws IOException
     */
    void writeCsv(final String response) throws IOException {

        final File file = new File("membersResponse.csv");
        FileWriter writer = null;
        CSVPrinter printer = null;

        if (!file.exists()) {
            writer = new FileWriter(file);
            printer = new CSVPrinter(writer, CSVFormat.DEFAULT);
            printer.printRecord("API", "Response", "Created At");
        } else {
            writer = new FileWriter(file, true);
            printer = new CSVPrinter(writer, CSVFormat.DEFAULT);
        }

        // create data rows
        printer.printRecord("Claim", response, new Timestamp(new Date().getTime()));

        //close the printer after the file is complete
        printer.flush();
        printer.close();
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setRoutingkey(String routingkey) {
        this.routingkey = routingkey;
    }
}
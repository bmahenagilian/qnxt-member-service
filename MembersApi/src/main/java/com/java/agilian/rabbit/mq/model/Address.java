package com.java.agilian.rabbit.mq.model;

public class Address {
    private String countryName;
    private String type;
    private String attentionTo;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String zipCode;
    private String county;
    private String postalCode;
    private String province;
    private String countryID;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String value) {
        this.countryName = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String value) {
        this.type = value;
    }

    public String getAttentionTo() {
        return attentionTo;
    }

    public void setAttentionTo(String value) {
        this.attentionTo = value;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String value) {
        this.addressLine1 = value;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String value) {
        this.addressLine2 = value;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String value) {
        this.city = value;
    }

    public String getState() {
        return state;
    }

    public void setState(String value) {
        this.state = value;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String value) {
        this.zipCode = value;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String value) {
        this.county = value;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String value) {
        this.province = value;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String value) {
        this.countryID = value;
    }
}
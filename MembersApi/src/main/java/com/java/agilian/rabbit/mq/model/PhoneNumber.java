package com.java.agilian.rabbit.mq.model;

public class PhoneNumber {
    private String type;
    private String number;

    public String getType() {
        return type;
    }

    public void setType(String value) {
        this.type = value;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String value) {
        this.number = value;
    }
}
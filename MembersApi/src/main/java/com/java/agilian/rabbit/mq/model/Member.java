package com.java.agilian.rabbit.mq.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author Buddha.
 * @Date 6/16/2022.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Member {

    private String ethnicity;
    private String language;
    private String secondaryID;
    private ContactInfo contactInfo;
    private List<MemberRelation> memberRelations;
    private ProcessMetadata processMetadata;
    private String genderIdentity;
    private String hasAccess;
    private String restrictedAccessGroups;
    private String memID;
    private String name;
    private String entityID;
    private String sex;
    private String maritalStatus;
    private String ssn;
    private Timestamp birthDate;
    private String headOfHouse;
    private String isVip;
    private String status;
    private Timestamp deathDate;
    private String ethnicityID;
    private String languageID;
    private long medianIncome;
    private String guardian;
    private String externalID;
    private String isMultipleBirth;
    private String isSubscriber;
    private String genderIdentityID;
    private String entityState;

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String value) {
        this.ethnicity = value;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String value) {
        this.language = value;
    }

    public String getSecondaryID() {
        return secondaryID;
    }

    public void setSecondaryID(String value) {
        this.secondaryID = value;
    }

    public ContactInfo getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(ContactInfo value) {
        this.contactInfo = value;
    }

    public List<MemberRelation> getMemberRelations() {
        return memberRelations;
    }

    public void setMemberRelations(List<MemberRelation> memberRelations) {
        this.memberRelations = memberRelations;
    }

    public ProcessMetadata getProcessMetadata() {
        return processMetadata;
    }

    public void setProcessMetadata(ProcessMetadata value) {
        this.processMetadata = value;
    }

    public String getGenderIdentity() {
        return genderIdentity;
    }

    public void setGenderIdentity(String value) {
        this.genderIdentity = value;
    }

    public String getHasAccess() {
        return hasAccess;
    }

    public void setHasAccess(String value) {
        this.hasAccess = value;
    }

    public String getRestrictedAccessGroups() {
        return restrictedAccessGroups;
    }

    public void setRestrictedAccessGroups(String value) {
        this.restrictedAccessGroups = value;
    }

    public String getMemID() {
        return memID;
    }

    public void setMemID(String value) {
        this.memID = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getEntityID() {
        return entityID;
    }

    public void setEntityID(String value) {
        this.entityID = value;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String value) {
        this.sex = value;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String value) {
        this.maritalStatus = value;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String value) {
        this.ssn = value;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp value) {
        this.birthDate = value;
    }

    public String getHeadOfHouse() {
        return headOfHouse;
    }

    public void setHeadOfHouse(String value) {
        this.headOfHouse = value;
    }

    public String getIsVip() {
        return isVip;
    }

    public void setIsVip(String value) {
        this.isVip = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String value) {
        this.status = value;
    }

    public Timestamp getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(Timestamp value) {
        this.deathDate = value;
    }

    public String getEthnicityID() {
        return ethnicityID;
    }

    public void setEthnicityID(String value) {
        this.ethnicityID = value;
    }

    public String getLanguageID() {
        return languageID;
    }

    public void setLanguageID(String value) {
        this.languageID = value;
    }

    public long getMedianIncome() {
        return medianIncome;
    }

    public void setMedianIncome(long value) {
        this.medianIncome = value;
    }

    public String getGuardian() {
        return guardian;
    }

    public void setGuardian(String value) {
        this.guardian = value;
    }

    public String getExternalID() {
        return externalID;
    }

    public void setExternalID(String value) {
        this.externalID = value;
    }

    public String getIsMultipleBirth() {
        return isMultipleBirth;
    }

    public void setIsMultipleBirth(String value) {
        this.isMultipleBirth = value;
    }

    public String getIsSubscriber() {
        return isSubscriber;
    }

    public void setIsSubscriber(String value) {
        this.isSubscriber = value;
    }

    public String getGenderIdentityID() {
        return genderIdentityID;
    }

    public void setGenderIdentityID(String value) {
        this.genderIdentityID = value;
    }

    public String getEntityState() {
        return entityState;
    }

    public void setEntityState(String value) {
        this.entityState = value;
    }


}

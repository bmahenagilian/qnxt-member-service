package com.java.agilian.rabbit.mq.service.impl;

import com.java.agilian.rabbit.mq.service.MemberMemoAlertService;
import com.java.agilian.rabbit.mq.util.HeadersUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static com.java.agilian.rabbit.mq.service.ApiConstants.GET_MEMBER_MEMOS;
import static com.java.agilian.rabbit.mq.service.ApiConstants.*;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * @author Buddha.
 * @Date 7/1/2022.
 */
@Service
public class MemberMemoAlertServiceImpl implements MemberMemoAlertService {

    final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private QnxtProducerServiceImpl producerService;

    @Override
    public String getMemberMemos(final String memberId) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(httpHeaders);

        final ResponseEntity<String> responseEntity = restTemplate.exchange(format(GET_MEMBER_MEMOS, memberId), HttpMethod.GET, httpEntity, String.class);

        if (isBlank(responseEntity.getBody())) {
            return EMPTY;
        }

        // sending to message queue.
        producerService.send(responseEntity.getBody());

        return responseEntity.getBody();
    }

    @Override
    public String updateMemberMemos(final String memberId, final String inputRequest) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(inputRequest, httpHeaders);

        final ResponseEntity<String> responseEntity = restTemplate.exchange(format(UPDATE_MEMBER_MEMOS, memberId), HttpMethod.PUT, httpEntity, String.class);

        if (isBlank(responseEntity.getBody())) {
            return EMPTY;
        }

        // sending to message queue.
        producerService.send(responseEntity.getBody());

        return responseEntity.getBody();
    }

    @Override
    public String getMemberAlerts(final String memberId, final List<String> expand) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(httpHeaders);

        final ResponseEntity<String> responseEntity = restTemplate.exchange(format(GET_MEMBER_ALERTS, memberId), HttpMethod.GET, httpEntity, String.class);

        if (isBlank(responseEntity.getBody())) {
            return EMPTY;
        }

        // sending to message queue.
        producerService.send(responseEntity.getBody());

        return responseEntity.getBody();
    }

    @Override
    public String updateMemberAlerts(final String memberId, final String inputRequest) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity httpEntity = new HttpEntity(inputRequest, httpHeaders);

        final ResponseEntity<String> responseEntity = restTemplate.exchange(format(UPDATE_MEMBER_ALERTS, memberId), HttpMethod.PUT, httpEntity, String.class);

        if (isBlank(responseEntity.getBody())) {
            return EMPTY;
        }

        // sending to message queue.
        producerService.send(responseEntity.getBody());

        return responseEntity.getBody();
    }
}

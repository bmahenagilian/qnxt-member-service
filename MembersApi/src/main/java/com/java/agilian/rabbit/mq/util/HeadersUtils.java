package com.java.agilian.rabbit.mq.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static com.java.agilian.rabbit.mq.service.ApiConstants.*;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Headers utility class to create http headers for api calls.
 *
 * @author Buddha.
 * @Date 5/22/2022.
 */
public class HeadersUtils {

    private static final Logger log = LoggerFactory.getLogger(HeadersUtils.class);
    private RestTemplate restTemplate = new RestTemplate();


    /**
     * Prepares http headers for the basic authorization api call for the given username and password.
     *
     * @param userName username.
     * @param password password.
     * @return http header.
     */
    public static HttpHeaders getBasicAuthenticationHeader(final String userName, final String password) {

        //creating basic authentication credentials.
        final String authStr = userName + ":" + password;// username:password
        final String base64Credentials = Base64.encodeBase64String(authStr.getBytes());

        //creating headers
        final HttpHeaders header = new HttpHeaders();
        header.add(AUTHORIZATION, "Basic " + base64Credentials);

        return header;
    }

    /**
     * Prepares http headers for the bearer token api call for the given bearer token.
     *
     * @param bearerToken bearer token.
     * @return http header.
     */
    public static HttpHeaders getBearerHeader(final String bearerToken) {

        // creating http header for bearer token.
        final HttpHeaders header = new HttpHeaders();
        header.add(AUTHORIZATION, "Bearer " + bearerToken);

        return header;
    }

    /**
     * Prepare http headers for api call.
     *
     * @return http header.
     * @throws Exception
     */
    public HttpHeaders getHttpHeaders() throws Exception {

        //creating headers
        final HttpHeaders headers = getBasicAuthenticationHeader("APIServAcct", "");
        headers.add(API_HEADER_NAME, API_HEADER_VALUE);

        //creating http entity
        final HttpEntity requestEntity = new HttpEntity(headers);

        // making api call
        final ResponseEntity<String> response = restTemplate.exchange(TOKEN_URL, HttpMethod.GET, requestEntity, String.class);

        if (isBlank(response.getBody())) {
            log.info("No response from access token api.");
            return null;
        }

        // converting response string to map.
        final Map<String, Object> responseMap = new ObjectMapper().readValue(response.getBody(), HashMap.class);

        if (!responseMap.containsKey(ACCESS_TOKEN)) {
            log.info("Response not contains access token..");
            return null;
        }

        final String accessToken = (String) responseMap.get(ACCESS_TOKEN);
        final HttpHeaders apiHeaders = getBearerHeader(accessToken);
        apiHeaders.add(API_HEADER_NAME, API_HEADER_VALUE);
        apiHeaders.setContentType(MediaType.APPLICATION_JSON);

        return apiHeaders;
    }
}

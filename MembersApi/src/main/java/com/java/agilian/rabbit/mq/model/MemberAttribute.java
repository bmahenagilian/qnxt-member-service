package com.java.agilian.rabbit.mq.model;

import java.sql.Timestamp;

public class MemberAttribute {
    private String attributeId;
    private String attributeValue;
    private Timestamp effDate;
    private Timestamp effDateOriginal;
    private Timestamp termDate;
    private String entityState;

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String value) {
        this.attributeId = value;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String value) {
        this.attributeValue = value;
    }

    public Timestamp getEffDate() {
        return effDate;
    }

    public void setEffDate(Timestamp effDate) {
        this.effDate = effDate;
    }

    public Timestamp getEffDateOriginal() {
        return effDateOriginal;
    }

    public void setEffDateOriginal(Timestamp effDateOriginal) {
        this.effDateOriginal = effDateOriginal;
    }

    public Timestamp getTermDate() {
        return termDate;
    }

    public void setTermDate(Timestamp termDate) {
        this.termDate = termDate;
    }

    public String getEntityState() {
        return entityState;
    }

    public void setEntityState(String value) {
        this.entityState = value;
    }
}

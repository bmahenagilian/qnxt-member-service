package com.java.agilian.rabbit.mq.model;

import java.util.List;

public class ContactInfo {
    private List<Address> addresses;
    private List<PhoneNumber> phoneNumbers;
    private EmergentContact emergentContact;
    private String email;
    private String contactName;

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public EmergentContact getEmergentContact() {
        return emergentContact;
    }

    public void setEmergentContact(EmergentContact value) {
        this.emergentContact = value;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String value) {
        this.contactName = value;
    }
}
package com.java.agilian.rabbit.mq.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import static com.java.agilian.rabbit.mq.util.HeadersUtils.getBasicAuthenticationHeader;
import static com.java.agilian.rabbit.mq.util.HeadersUtils.getBearerHeader;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
public class QnxtProducerServiceImpl {

    public static final String ACCESS_TOKEN = "access_token";
    public static final String API_HEADER_NAME = "X-TZ-EnvId";
    public static final String API_HEADER_VALUE = "1";
    private static final Logger log = LoggerFactory.getLogger(QnxtProducerServiceImpl.class);
    private static final String TOKEN_URL = "https://b63qnxthubreg3.cishoc.com:12808/QNXTsts";
    private static final String MEMBER_API_URL = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Member/members/%S";
    @Value("${spring.rabbitmq.username}")
    private String username;
    @Value("${spring.rabbitmq.password}")
    private String pwd;
    @Value("${spring.rabbitmq.host}")
    private String host;
    @Value("${spring.rabbitmq.exchange}")
    private String exchange;
    @Value("${spring.rabbitmq.port}")
    private Integer port;
    @Value("${spring.rabbitmq.routingkey}")
    private String routingkey;

    private final ConnectionFactory factory = new ConnectionFactory();

    private final RestTemplate restTemplate = new RestTemplate();

    public void send(final String message) throws NoSuchAlgorithmException, KeyManagementException, IOException, TimeoutException {

        factory.setUsername(username);
        factory.setPassword(pwd);

        factory.setHost(host);
        factory.setPort(port);
        // Allows client to establish a connection over TLS
        factory.useSslProtocol("TLSv1.2");

        // Create a connection
        final Connection conn = factory.newConnection();

        // Create a channel
        final Channel channel = conn.createChannel();
        final byte[] messageBodyBytes = message.getBytes();
        channel.basicPublish(exchange, routingkey, new AMQP.BasicProperties.Builder().contentType("text/plain").userId(username).build(), message.getBytes());

        log.info("Send msg = " + message);
    }

    public String getMember(final String memberId) throws Exception {

        //creating headers
        final HttpHeaders headers = getBasicAuthenticationHeader("APIServAcct", "");
        headers.add(API_HEADER_NAME, API_HEADER_VALUE);

        //creating http entity
        final HttpEntity requestEntity = new HttpEntity(headers);

        // making api call
        final ResponseEntity<String> response = restTemplate.exchange(TOKEN_URL, HttpMethod.GET, requestEntity, String.class);

        if (isBlank(response.getBody())) {
            return EMPTY;
        }

        // converting response string to map.
        final Map<String, Object> responseMap = new ObjectMapper().readValue(response.getBody(), HashMap.class);

        if (!responseMap.containsKey(ACCESS_TOKEN)) {
            log.info("Response not contains access token..");
            return "";
        }

        final String accessToken = (String) responseMap.get(ACCESS_TOKEN);
        final HttpHeaders apiHeaders = getBearerHeader(accessToken);
        apiHeaders.add(API_HEADER_NAME, API_HEADER_VALUE);

        final HttpEntity httpEntity = new HttpEntity(apiHeaders);
        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(MEMBER_API_URL, memberId), HttpMethod.GET, httpEntity, String.class);
        log.info("Fetched Member Data for Member id : " + memberId);

        final String responseBody = apiResponse.getBody();

        // sending to message queue.
        send(responseBody);

        return responseBody;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public void setRoutingkey(String routingkey) {
        this.routingkey = routingkey;
    }
}
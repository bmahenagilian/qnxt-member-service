package com.java.agilian.rabbit.mq.service;

/**
 * Api Constants.
 *
 * @author Buddha.
 * @Date 6/28/2022.
 */
public interface ApiConstants {

    // Http Entity & Header Values.
    String ACCESS_TOKEN = "access_token";
    String API_HEADER_NAME = "X-TZ-EnvId";
    String API_HEADER_VALUE = "1";
    String AUTHORIZATION = "Authorization";
    String TOKEN_URL = "https://b63qnxthubreg3.cishoc.com:12808/QNXTsts";

    // Member api urls
    String GET_MEMBER = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Member/members/%S";


    // Member attributes api urls
    String SEARCH_MEMBER_ATTRIBUTES = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Member/members/%S/attributes/search";
    String UPDATE_MEMBER_ATTRIBUTES = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Member/members/%S/attributes";

    // Member memos & alerts api urls
    String GET_MEMBER_MEMOS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Member/members/%S/memoAlerts/memos";
    String UPDATE_MEMBER_MEMOS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Member/members/%S/memoAlerts/memos";

    String GET_MEMBER_ALERTS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Member/members/%S/memoAlerts/alerts";
    String UPDATE_MEMBER_ALERTS = "https://b63qnxthubreg3.cishoc.com:12808/QNXTApi/Member/members/%S/memoAlerts/alerts";
}

package com.java.agilian.rabbit.mq.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Buddha.
 * @Date 6/16/2022.
 */
@Entity
@Table(name = "member_mom")
public class MemberEntity {

    @Id
    @Column(name = "id")
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

package com.java.agilian.rabbit.mq.model;

public class ProcessMessage {
    private String messageID;
    private String message;
    private String severity;
    private String category;
    private String source;
    private AdditionalInfo additionalInfo;

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String value) {
        this.messageID = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String value) {
        this.message = value;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String value) {
        this.severity = value;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String value) {
        this.category = value;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String value) {
        this.source = value;
    }

    public AdditionalInfo getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(AdditionalInfo value) {
        this.additionalInfo = value;
    }
}
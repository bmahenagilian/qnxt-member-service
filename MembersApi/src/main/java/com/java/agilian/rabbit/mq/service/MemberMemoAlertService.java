package com.java.agilian.rabbit.mq.service;

import java.util.List;

/**
 * @author Buddha.
 * @Date 7/1/2022.
 */
public interface MemberMemoAlertService {

    /**
     * Gets member memos for the given member id.
     *
     * @param memberId member id
     * @return member memos.
     * @throws Exception exception.
     */
    public String getMemberMemos(final String memberId) throws Exception;

    /**
     * Updates the member memos for the given member id.
     *
     * @param memberId     member id.
     * @param inputRequest input request.
     * @return return response.
     * @throws Exception exception.
     */
    public String updateMemberMemos(final String memberId, final String inputRequest) throws Exception;

    /**
     * Gets member alerts for the given member id.
     *
     * @param memberId member id.
     * @param expand   expand.
     * @return member alerts.
     * @throws Exception exception.
     */
    public String getMemberAlerts(final String memberId, final List<String> expand) throws Exception;

    /**
     * Updates the member alerts for the given member id.
     *
     * @param memberId     member id.
     * @param inputRequest input request.
     * @return return response.
     * @throws Exception exception.
     */
    public String updateMemberAlerts(final String memberId, final String inputRequest) throws Exception;
}

package com.java.agilian.rabbit.mq.service.impl;

import com.java.agilian.rabbit.mq.model.MemberAttributeSearch;
import com.java.agilian.rabbit.mq.service.MemberAttributeService;
import com.java.agilian.rabbit.mq.util.HeadersUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.java.agilian.rabbit.mq.service.ApiConstants.SEARCH_MEMBER_ATTRIBUTES;
import static com.java.agilian.rabbit.mq.service.ApiConstants.UPDATE_MEMBER_ATTRIBUTES;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Implementation of {@link com.java.agilian.rabbit.mq.service.MemberAttributeService}
 *
 * @author Buddha.
 * @Date 6/30/2022.
 */
@Service
public class MemberAttributeServiceImpl implements MemberAttributeService {

    private static final Logger log = LoggerFactory.getLogger(MemberAttributeServiceImpl.class);

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private QnxtProducerServiceImpl producerService;

    @Override
    public String searchMemberAttributes(final String memberId, final MemberAttributeSearch searchCriteria) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(SEARCH_MEMBER_ATTRIBUTES, memberId), HttpMethod.GET, new HttpEntity<>(httpHeaders), String.class);

        if (isEmpty(apiResponse.getBody())) {
            return EMPTY;
        }

        producerService.send(apiResponse.getBody());

        return apiResponse.getBody();
    }

    @Override
    public String updateMemberAttributes(final String memberId, final String inputRequest) throws Exception {

        final HttpHeaders httpHeaders = new HeadersUtils().getHttpHeaders();

        if (httpHeaders == null) {
            return null;
        }

        final HttpEntity<String> httpEntity = new HttpEntity<>(inputRequest, httpHeaders);

        final ResponseEntity<String> apiResponse = restTemplate.exchange(format(UPDATE_MEMBER_ATTRIBUTES, memberId), HttpMethod.PUT, httpEntity, String.class);

        if (isEmpty(apiResponse.getBody())) {
            return EMPTY;
        }

        producerService.send(apiResponse.getBody());

        return apiResponse.getBody();
    }
}

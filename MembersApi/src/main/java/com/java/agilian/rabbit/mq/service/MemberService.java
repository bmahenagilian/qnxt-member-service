package com.java.agilian.rabbit.mq.service;

import com.java.agilian.rabbit.mq.dto.MemberDto;

/**
 * Member service that process member api's.
 *
 * @author Buddha.
 * @Date 6/30/2022.
 */
public interface MemberService {

    /**
     * Gets the member for the given member id.
     *
     * @param memberId  member id.
     * @param memberDto member dto.
     * @return returns member.
     * @throws Exception exception.
     */
    String getMemberById(final String memberId, final MemberDto memberDto) throws Exception;
}

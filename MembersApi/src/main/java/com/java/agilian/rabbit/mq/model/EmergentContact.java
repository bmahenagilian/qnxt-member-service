package com.java.agilian.rabbit.mq.model;

public class EmergentContact {
    private String name;
    private String relationship;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String value) {
        this.relationship = value;
    }
}